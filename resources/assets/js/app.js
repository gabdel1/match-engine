var setup = {
    "rules": {
        "id": 22,
        "nome": "Tag Team",
        "desc": 2284,
        "durata": 150,
        "vittoria": {
            "pin": true,
            "sub": true,
            "infortunio_5": true,
            "sanguinamento_5": true,
            "fuori_ring": true
        },
        "vittoria_rapida_squadra": true,
        "vittoria_rapida_incontro": true,
        "prob_uso_oggetti": 0,
        "ids_mosse_oggetti": "[565, 566, 567, 568, 569, 570, 571, 572, 573]",
        "fuori_ring": true,
        "cambio": true,
        "interventi_scorretti": true,
        "interventi_molto_scorretti": false,
        "n_squadre_attive_inizio": null,
        "D_turni_ingresso": null,
        "prob_ingresso_a_squadre": 0,
        "max_squadre_attive": null,
        "annuncia_panchinari": false,
        "entrance_panchinari": false
    },
    "teams": {
        "819": {
            "tipo": "tagteam",
            "tipologia": "giocante",
            "nome": "Bobby and Due",
            "attivi": 1,
            "fighters_sopravvissuti": [
                410499,
                456832
            ]
        },
        "3624": {
            "tipo": "tagteam",
            "tipologia": "giocante",
            "nome": "bossesss",
            "attivi": 1,
            "fighters_sopravvissuti": [
                712076,
                745379
            ]
        }
    },
    "wrestlers": {
        "410499": {
            "nome": "Bobby Pooks Jr",
            "tipologia": "giocante",
            "livello": 12,
            "img_profilo": "http://www.thewrestlinggame.com/wg/img/class/2.jpg",
            "nomi_classi": [
                "speed",
                "technique"
            ],
            "iniziativa": 51,
            "vita_max": 108,
            "stamina_max": 52,
            "bonus_adrenalina": 2,
            "bonus_sangue": 1,
            "attacco": 10,
            "parata": 1,
            "schivata": 3,
            "sottomissione": 1,
            "difesa_sottomissione": 2,
            "danno_attacco": 3,
            "danno_sottomissione": 1,
            "danno_riduzione": 1,
            "pin": 1,
            "difesa_pin": 1,
            "riduzione_stamina": 2,
            "max_attacchi": 1,
            "bonus_sequenza_attacco": 6,
            "difesa_max_attacchi": 0,
            "difesa_sequenza_attacco": 5,
            "prob_intervento_scorretto": 100,
            "extra": {
                "special_skill": []
            },
            "mosse": {
                "11": 19,
                "29": 19,
                "30": 16,
                "39": 2,
                "49": 19,
                "62": 2,
                "63": 19,
                "66": 3,
                "69": 15,
                "71": 2,
                "78": 3,
                "93": 2,
                "109": 6,
                "117": 6,
                "118": 6,
                "129": 6,
                "135": 5,
                "149": 4,
                "153": 3,
                "159": 10,
                "169": 4,
                "171": 8,
                "175": 2,
                "189": 2,
                "211": 6,
                "219": 6,
                "224": 10,
                "229": 10,
                "254": 2,
                "340": 6,
                "347": 3,
                "361": 3,
                "377": 2,
                "380": 16,
                "387": 15,
                "390": 3,
                "412": 3,
                "417": 19,
                "420": 3,
                "422": 2,
                "424": 2,
                "425": 2,
                "430": 30,
                "433": 9,
                "440": 6,
                "453": 3,
                "454": 2,
                "472": 4,
                "482": 4,
                "491": 4,
                "497": 4,
                "505": 6,
                "517": 7,
                "522": 7,
                "2004": 1,
                "2005": 1,
                "2007": 1
            },
            "finisher": [
                {
                    "nome_id": "Hammer",
                    "livello": 1,
                    "before": "[fighter] Lifts the opponent on his shoulders",
                    "after": "and slams [opponent] on the mat executing a [move]",
                    "block": 554,
                    "miss": 555,
                    "id": 120309,
                    "pos_richiesta": 16,
                    "pos_ottenuta": 5,
                    "pos_subita": 16,
                    "attacco": 10,
                    "danno_min": 185.08,
                    "danno_max": 185.08,
                    "bonus_pin": 1,
                    "bonus_sub": 1,
                    "bonus_sangue": 1,
                    "mods": {
                        "adrenalina": 650,
                        "vitalita_max_avversario": 100
                    }
                },
                {
                    "nome_id": "Lasso",
                    "livello": 1,
                    "before": "[fighter] Lifts the opponent on his shoulders",
                    "after": "and slams [opponent] on the mat executing a [move]",
                    "block": 554,
                    "miss": 555,
                    "id": 120329,
                    "pos_richiesta": 4,
                    "pos_ottenuta": 20,
                    "pos_subita": 16,
                    "attacco": 10,
                    "danno_min": 9,
                    "danno_max": 9,
                    "bonus_pin": 88.11,
                    "bonus_sub": 88.11,
                    "bonus_sangue": 0,
                    "mods": {
                        "adrenalina": 529,
                        "vitalita_max_avversario": 100
                    }
                },
                {
                    "nome_id": "Rubble",
                    "livello": 1,
                    "before": "[fighter] Lifts the opponent on his shoulders",
                    "after": "and slams [opponent] on the mat executing a [move]",
                    "block": 554,
                    "miss": 555,
                    "id": 120310,
                    "pos_richiesta": 3,
                    "pos_ottenuta": 20,
                    "pos_subita": 16,
                    "attacco": 10,
                    "danno_min": 10,
                    "danno_max": 10,
                    "bonus_pin": 54,
                    "bonus_sub": 54,
                    "bonus_sangue": 0,
                    "mods": {
                        "adrenalina": 800,
                        "vitalita_max_avversario": 100
                    }
                }
            ],
            "trademark": [
                {
                    "mossa_id": 85,
                    "id": 53797,
                    "livello": 33,
                    "nome_id": "Icicle "
                },
                {
                    "mossa_id": 424,
                    "id": 55089,
                    "livello": 19,
                    "nome_id": "Thunderbolt"
                },
                {
                    "mossa_id": 434,
                    "id": 53138,
                    "livello": 55,
                    "nome_id": "Anaconda Lock "
                }
            ],
            "taunt": [
                {
                    "nome_id": "Waiting for a century",
                    "id": 11560,
                    "livello": 45,
                    "turno_1": "",
                    "turno_2": "",
                    "turno_3": ""
                }
            ],
            "specialization": {
                "match": {
                    "17": {
                        "avversario": {
                            "attacco": -5,
                            "parata": -5,
                            "schivata": -5,
                            "danno_attacco": -5,
                            "danno_sottomissione": -5,
                            "danno_riduzione": -5,
                            "riduzione_stamina": -5,
                            "bonus_adrenalina": -5,
                            "bonus_sangue": -5
                        }
                    }
                },
                "class": {
                    "strength": {
                        "avversario": {
                            "danno_attacco": -10
                        }
                    },
                    "speed": {
                        "avversario": {
                            "attacco": -10
                        }
                    },
                    "technique": {
                        "avversario": {
                            "sottomissione": -10
                        }
                    }
                }
            },
            "entrance": [
                {
                    "nome_id": 3064,
                    "annuncio": 30003,
                    "livello": 100
                },
                {
                    "nome_id": 3066,
                    "annuncio": 30006,
                    "livello": 41
                },
                {
                    "nome_id": 78927,
                    "annuncio": 30009,
                    "livello": 7
                },
                {
                    "nome_id": 78928,
                    "annuncio": 30012,
                    "livello": 7
                },
                {
                    "nome_id": 3063,
                    "annuncio": 30015,
                    "livello": 7
                },
                {
                    "nome_id": 3065,
                    "annuncio": 30018,
                    "livello": 7
                },
                {
                    "nome_id": 3067,
                    "annuncio": 30021,
                    "livello": 7
                }
            ]
        },
        "456832": {
            "nome": "Duenerth",
            "tipologia": "giocante",
            "livello": 9,
            "img_profilo": "http://www.thewrestlinggame.com/wg/img/class/1.jpg",
            "nomi_classi": [
                "strength"
            ],
            "iniziativa": 51,
            "vita_max": 75,
            "stamina_max": 21,
            "bonus_adrenalina": 4,
            "bonus_sangue": 1,
            "attacco": 4,
            "parata": 1,
            "schivata": 2,
            "sottomissione": 1,
            "difesa_sottomissione": 2,
            "danno_attacco": 5,
            "danno_sottomissione": 1,
            "danno_riduzione": 3,
            "pin": 1,
            "difesa_pin": 1,
            "riduzione_stamina": 1,
            "max_attacchi": 0,
            "bonus_sequenza_attacco": 2,
            "difesa_max_attacchi": 0,
            "difesa_sequenza_attacco": 2,
            "prob_intervento_scorretto": 100,
            "extra": {
                "special_skill": []
            },
            "mosse": {
                "10": 33,
                "11": 34,
                "13": 35,
                "25": 11,
                "31": 24,
                "36": 8,
                "46": 27,
                "55": 3,
                "62": 3,
                "66": 11,
                "69": 26,
                "71": 4,
                "78": 3,
                "79": 4,
                "129": 10,
                "153": 11,
                "159": 27,
                "164": 3,
                "166": 9,
                "167": 10,
                "172": 10,
                "178": 9,
                "182": 4,
                "189": 11,
                "193": 5,
                "196": 7,
                "200": 4,
                "205": 15,
                "212": 13,
                "213": 7,
                "218": 22,
                "220": 5,
                "238": 17,
                "324": 5,
                "361": 14,
                "364": 24,
                "377": 8,
                "380": 29,
                "412": 3,
                "425": 9,
                "433": 9,
                "482": 7,
                "484": 5,
                "485": 4,
                "486": 2,
                "505": 8,
                "517": 8,
                "2004": 1,
                "2005": 1,
                "2007": 1
            },
            "finisher": [
                {
                    "nome_id": "Freuz Maker",
                    "livello": 1,
                    "before": "[fighter] lifts the opponent on his shoulders and grabs his legs,",
                    "after": "then he starts walking and slams his opponent on the mat performing the [move]",
                    "block": 554,
                    "miss": 555,
                    "id": 118572,
                    "pos_richiesta": 4,
                    "pos_ottenuta": 5,
                    "pos_subita": 6,
                    "attacco": 16,
                    "danno_min": 328.22,
                    "danno_max": 375.12,
                    "bonus_pin": 1,
                    "bonus_sub": 1,
                    "bonus_sangue": 1,
                    "mods": {
                        "adrenalina": 1200,
                        "vitalita_max_avversario": 100
                    }
                }
            ],
            "trademark": [
                {
                    "mossa_id": 159,
                    "id": 54372,
                    "livello": 30,
                    "nome_id": "Celtic Buster "
                },
                {
                    "mossa_id": 218,
                    "id": 54162,
                    "livello": 20,
                    "nome_id": "Veneti Curse "
                },
                {
                    "mossa_id": 189,
                    "id": 54803,
                    "livello": 10,
                    "nome_id": "Modified Polish Hammer"
                }
            ],
            "taunt": [
                {
                    "nome_id": "Skizhañ",
                    "id": 11069,
                    "livello": 36,
                    "turno_1": "[fighter] gives a mean look to the opponent and shouts something in old celtic to his face, performing a Skizhañ taunt."
                }
            ],
            "specialization": {
                "class": {}
            },
            "entrance": [
                {
                    "nome_id": 3155,
                    "annuncio": 30003,
                    "livello": 5
                },
                {
                    "nome_id": 3156,
                    "annuncio": 30006,
                    "livello": 5
                },
                {
                    "nome_id": 126913,
                    "annuncio": 30009,
                    "livello": 5
                },
                {
                    "nome_id": 126914,
                    "annuncio": 30012,
                    "livello": 5
                },
                {
                    "nome_id": 3158,
                    "annuncio": 30018,
                    "livello": 5
                },
                {
                    "nome_id": 3157,
                    "annuncio": 30021,
                    "livello": 4
                }
            ]
        },
        "712076": {
            "nome": "MAXOLINO 2ND JR",
            "tipologia": "giocante",
            "livello": 11,
            "img_profilo": "https://world-it.thewrestlinggame.com/wg/img/wrestler/712076.jpg",
            "nomi_classi": [
                "balanced",
                "strength",
                "resistance"
            ],
            "iniziativa": 11432,
            "vita_max": 118060,
            "stamina_max": 8463,
            "bonus_adrenalina": 22,
            "bonus_sangue": 22,
            "attacco": 31200,
            "parata": 236,
            "schivata": 32,
            "sottomissione": 26,
            "difesa_sottomissione": 142,
            "danno_attacco": 383,
            "danno_sottomissione": 3338,
            "danno_riduzione": 15112,
            "pin": 26,
            "difesa_pin": 15,
            "riduzione_stamina": 22,
            "max_attacchi": 7,
            "bonus_sequenza_attacco": 33,
            "difesa_max_attacchi": 3,
            "difesa_sequenza_attacco": 11,
            "prob_intervento_scorretto": 100,
            "extra": {
                "special_skill": []
            },
            "mosse": {
                "12": 18,
                "13": 17,
                "25": 12,
                "26": 66,
                "27": 17,
                "28": 11,
                "29": 45,
                "30": 71,
                "33": 13,
                "56": 15,
                "146": 15,
                "152": 56,
                "172": 11,
                "201": 15,
                "209": 46,
                "215": 6,
                "287": 17,
                "361": 11,
                "377": 12,
                "393": 17,
                "401": 11,
                "409": 11,
                "425": 7,
                "430": 15,
                "433": 11,
                "477": 6,
                "482": 6,
                "488": 4,
                "499": 4,
                "509": 4,
                "515": 6,
                "517": 4,
                "522": 4,
                "528": 4
            },
            "finisher": [
                {
                    "nome_id": "Finisher",
                    "livello": 1,
                    "before": "[fighter] Solleva l'avversario sulle sue spalle",
                    "after": 553,
                    "block": 554,
                    "miss": 555,
                    "id": 124106,
                    "pos_richiesta": 5,
                    "pos_ottenuta": 5,
                    "pos_subita": 16,
                    "attacco": 10,
                    "danno_min": 100,
                    "danno_max": 500,
                    "bonus_pin": 0,
                    "bonus_sub": 0,
                    "bonus_sangue": 0,
                    "mods": {
                        "adrenalina": 2140,
                        "vitalita_max_avversario": 100
                    }
                },
                {
                    "nome_id": "Finisher",
                    "livello": 1,
                    "before": "[fighter] Solleva l'avversario sulle sue spalle",
                    "after": 553,
                    "block": 554,
                    "miss": 555,
                    "id": 124105,
                    "pos_richiesta": 4,
                    "pos_ottenuta": 5,
                    "pos_subita": 16,
                    "attacco": 10,
                    "danno_min": 100,
                    "danno_max": 200,
                    "bonus_pin": 0,
                    "bonus_sub": 0,
                    "bonus_sangue": 0,
                    "mods": {
                        "adrenalina": 1040,
                        "vitalita_max_avversario": 100
                    }
                }
            ],
            "trademark": [
                {
                    "mossa_id": 30,
                    "id": 59518,
                    "livello": 23,
                    "nome_id": "Forearm Smash Modificata"
                }
            ],
            "taunt": [
                {
                    "nome_id": "the executioner",
                    "id": 15665,
                    "livello": 17,
                    "turno_1": "[fighter] da un'occhiata all'avversario che sta eseguendo the executioner"
                }
            ],
            "specialization": {
                "match": {
                    "1": {
                        "avversario": {
                            "attacco": -50,
                            "parata": -50,
                            "schivata": -50,
                            "danno_attacco": -50,
                            "danno_sottomissione": -50,
                            "danno_riduzione": -50,
                            "riduzione_stamina": -50,
                            "bonus_adrenalina": -50,
                            "bonus_sangue": -50
                        }
                    },
                    "2": {
                        "avversario": {
                            "attacco": -70,
                            "parata": -70,
                            "schivata": -70,
                            "danno_attacco": -70,
                            "danno_sottomissione": -70,
                            "danno_riduzione": -70,
                            "riduzione_stamina": -70,
                            "bonus_adrenalina": -70,
                            "bonus_sangue": -70
                        }
                    },
                    "9": {
                        "avversario": {
                            "attacco": -50,
                            "parata": -50,
                            "schivata": -50,
                            "danno_attacco": -50,
                            "danno_sottomissione": -50,
                            "danno_riduzione": -50,
                            "riduzione_stamina": -50,
                            "bonus_adrenalina": -50,
                            "bonus_sangue": -50
                        }
                    }
                },
                "class": {}
            },
            "entrance": []
        },
        "745379": {
            "nome": "Abdel",
            "tipologia": "giocante",
            "livello": 3,
            "img_profilo": "http://www.thewrestlinggame.com/wg/img/class/1.jpg",
            "nomi_classi": [
                "strength",
                "technique"
            ],
            "iniziativa": 65,
            "vita_max": 660,
            "stamina_max": 605,
            "bonus_adrenalina": 26,
            "bonus_sangue": 5,
            "attacco": 73,
            "parata": 24,
            "schivata": 23,
            "sottomissione": 7,
            "difesa_sottomissione": 11,
            "danno_attacco": 81,
            "danno_sottomissione": 11,
            "danno_riduzione": 22,
            "pin": 227,
            "difesa_pin": 7,
            "riduzione_stamina": 14,
            "max_attacchi": 5,
            "bonus_sequenza_attacco": 5,
            "difesa_max_attacchi": 0,
            "difesa_sequenza_attacco": 3,
            "prob_intervento_scorretto": 100,
            "extra": {
                "special_skill": []
            },
            "mosse": {
                "10": 8,
                "11": 8,
                "25": 7,
                "26": 25,
                "28": 7,
                "29": 27,
                "30": 24,
                "49": 6,
                "69": 7,
                "85": 4,
                "93": 3,
                "129": 5,
                "147": 3,
                "150": 4,
                "151": 5,
                "152": 23,
                "154": 3,
                "160": 3,
                "170": 3,
                "172": 7,
                "175": 3,
                "189": 4,
                "193": 3,
                "197": 3,
                "206": 3,
                "209": 8,
                "212": 4,
                "213": 4,
                "234": 7,
                "238": 3,
                "361": 7,
                "377": 6,
                "401": 6,
                "409": 8,
                "425": 7,
                "433": 8,
                "482": 3,
                "488": 12,
                "489": 3,
                "515": 3,
                "517": 3,
                "522": 3,
                "528": 3
            },
            "finisher": [],
            "trademark": [],
            "taunt": [],
            "specialization": {
                "class": {}
            },
            "entrance": []
        }
    }
};

document.getElementById('send_match').addEventListener("click", function(){


    fetch("/api/team", {
        method: "POST",
        body: JSON.stringify(setup)
    }).then(function(response){
        return response.json();
    }).then(function(data){
        console.log(data);
    }); 

});
