<?php

namespace App\Engine\Foundation;

use App\Engine\Entities\Team;
use App\Engine\Entities\Wrestler;


class Parser{

    public function __construct(object $setup){
        $this->parseTeams($setup->teams);
        $this->parseWrestlers($setup->wrestlers);
    }
    
    protected function parseTeams(object $teams)
    {
        foreach ($teams as $key => $team) {
            $this->teams[] = new Team($team, $key);
        }
    }


    protected function parseWrestlers(object $wrestlers)
    {
        foreach ($wrestlers as $key => $wrestler) {
            $this->wrestlers[] = new Wrestler($wrestler, $key);
        }
    }


}