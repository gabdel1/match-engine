<?php

namespace App\Engine\Entities;

class Team
{
    protected $id;
    protected $nome;
    protected $attivi;
    protected $fighters_sopravvissuti;
    protected $tipo;
    protected $tipologia;

    protected $eliminated = false;

    public function __construct(object $team, int $id){
        
        $this->id = $id;
        foreach($team as $key => $value) {
            $this->$key = $value;
        }
    }
}