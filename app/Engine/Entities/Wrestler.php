<?php

namespace App\Engine\Entities;

class Wrestler {
    
    protected $id;
    protected $attacco;
    protected $bonus_adrenalina;
    protected $bonus_sequenza_attacco;
    protected $danno_attacco;
    protected $danno_riduzione;
    protected $difesa_max_attacchi;
    protected $difesa_pin;
    protected $difesa_sequenza_attacco;
    protected $difesa_sottomissione;
    protected $bonus_sangue;
    protected $danno_sottomissione;
    protected $iniziativa;
    protected $livello;
    protected $max_attacchi; 
    protected $nomi_classi;
    protected $nome;
    protected $parata;
    protected $pin;
    protected $riduzione_stamina;
    protected $schivata;
    protected $sottomissione;
    protected $stamina_max;
    protected $vita_max;
    protected $prob_intervento_scorretto;
    protected $extra;
    protected $mosse;
    protected $finisher;
    protected $trademark;
    protected $taunt;
    protected $specialization;
    protected $entrance;

    
    public function __construct(object $wrestler, int $id){

        $this->id = $id;
        foreach ($wrestler as $key => $value) {
            $this->$key = $value;
        }
    }


}


